//
//  ViewFactory.h
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/6/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewFactory : NSObject

+ (instancetype)sharedInstance;

- (UIViewController*)createFirstViewController;
- (UIViewController*)createSearchViewController;
- (UIViewController*)createMapsViewControllerWithData:(NSDictionary *) data;

@end
