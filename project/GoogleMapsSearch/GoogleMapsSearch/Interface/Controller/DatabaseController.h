//
//  DatabaseController.h
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/8/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseController : NSObject


+ (instancetype)sharedInstance;

- (void)listLocations;
- (void)insertLocation:(NSDictionary *) aLocation;
- (void)deleteLocation:(NSDictionary *) aLocation;
- (Boolean)isLocationSaved:(NSDictionary *) aLocation;

@end
