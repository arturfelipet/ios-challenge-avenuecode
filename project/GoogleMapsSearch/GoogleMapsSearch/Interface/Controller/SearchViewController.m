//
//  SearchViewController.m
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>{
    Boolean showDisplayAll;
}

@property NSMutableArray *objects;


@property (weak, nonatomic) IBOutlet UISearchBar *searchBarGoogle;
@property (weak, nonatomic) IBOutlet UITableView *tableViewSearchResult;
@property (weak, nonatomic) IBOutlet UILabel *labelNoResult;

@end

@implementation SearchViewController

+(void)load
{
    [super load];
}

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    DLog(@"viewDidLoad: %@", NSStringFromClass([self class]));
    
    [self.tableViewSearchResult setDelegate:self];
    [self.tableViewSearchResult setDataSource:self];
    
    [self.searchBarGoogle setDelegate:self];
    
    [self.tableViewSearchResult setHidden:YES];
    [self.labelNoResult setHidden:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    DLog(@"viewDidUnload: %@", NSStringFromClass([self class]));
}


- (void)viewWillAppear:(BOOL)animated {
    //    self.TableclearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    DLog(@"viewWillAppear: %@", NSStringFromClass([self class]));
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    DLog(@"viewDidAppear: %@", NSStringFromClass([self class]));
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    DLog(@"viewWillDisappear: %@", NSStringFromClass([self class]));
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    DLog(@"viewDidDisappear: %@", NSStringFromClass([self class]));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    
    NSString *searchTerm = self.searchBarGoogle.text;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false", [searchTerm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url];
    request.HTTPMethod = @"GET";
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"error: %@", error.localizedDescription);
        } else {
            NSError *jsonError;
            NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
            NSLog(@"%@", jsonDictionary);
            
            NSArray *results = [jsonDictionary objectForKey:@"results"];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.objects = [[NSMutableArray alloc] init];
                
                showDisplayAll = NO;
                
                if(results.count > 0){
                    
                    
                    [self.tableViewSearchResult setHidden:NO];
                    [self.labelNoResult setHidden:YES];
                    
                    if(results.count >= 2){
                        showDisplayAll = YES;
                    }
                    
                    for (NSDictionary *result in results) {
                        DLog(@"Display results here");
                        
                        [self.objects addObject:result];
                    }
                }
                else{
                    //Show Message No Results and hide the tableview.
                    [self.tableViewSearchResult setHidden:YES];
                    [self.labelNoResult setHidden:NO];
                }
                
                [self.tableViewSearchResult reloadData];
            });
        }
    }]
     resume];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(showDisplayAll){
        return 2;
    }
    else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(showDisplayAll && section == 0){
        return 1;
    }
    
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if(showDisplayAll && indexPath.section == 0){
        cell.textLabel.text = @"Display All on Map";
    }
    else{
        NSDictionary *aDict = self.objects[indexPath.row];
        cell.textLabel.text = [aDict objectForKey:@"formatted_address"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSMutableArray *marks = [[NSMutableArray alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    for(NSDictionary *aDict in self.objects){
        [marks addObject:aDict];
    }
    [data setObject:marks forKey:@"marks"];
    
    if(showDisplayAll && indexPath.row == 0 && indexPath.section == 0){ //Display All Selected
        [data setObject:@"1" forKey:@"displayAll"];
        [data setObject:@"0"  forKey:@"selectedIndex"];
    }
    else{
        [data setObject:@"0" forKey:@"displayAll"];
        [data setObject:[NSString stringWithFormat:@"%ld", (long)indexPath.row]  forKey:@"selectedIndex"];
    }
    
    UIViewController *controller = [[ViewFactory sharedInstance] createMapsViewControllerWithData:data];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

@end
