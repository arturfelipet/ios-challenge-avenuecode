//
//  MapsViewController.m
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import "MapsViewController.h"
#import "DatabaseController.h"

@interface MapsViewController () <GMSMapViewDelegate, UIAlertViewDelegate>{
    BOOL _firstLocationUpdate;
    
}

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) GMSMarker *locationMarker;
@property (strong, nonatomic) NSMutableArray *markers;

@end

@implementation MapsViewController


+(void)load
{
    [super load];
}

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    DLog(@"viewDidLoad: %@", NSStringFromClass([self class]));
    
    self.title = @"All Results";
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.868
                                                            longitude:151.2086
                                                                 zoom:12];
    
    [self prefersStatusBarHidden];
    self.mapView.myLocationEnabled = NO;
    self.mapView.camera = camera;
    self.mapView.delegate = self;
    self.mapView.settings.compassButton = NO;
    self.mapView.settings.myLocationButton = NO;
    
    [self createLocationMarks];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    DLog(@"viewDidUnload: %@", NSStringFromClass([self class]));
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    DLog(@"viewWillAppear: %@", NSStringFromClass([self class]));
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    DLog(@"viewDidAppear: %@", NSStringFromClass([self class]));
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    DLog(@"viewWillDisappear: %@", NSStringFromClass([self class]));
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    DLog(@"viewDidDisappear: %@", NSStringFromClass([self class]));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    self.locationMarker = nil;
    self.markers = nil;
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void) createLocationMarks{
    self.markers = [[NSMutableArray alloc] init];
    
    NSArray *marks = [self.data objectForKey:@"marks"];
    
    for(NSDictionary *aDict in marks){
        GMSMarker *marker = [[GMSMarker alloc] init];
        CLLocationDegrees lat = [[[[aDict objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lat"] doubleValue];
        CLLocationDegrees lng = [[[[aDict objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lng"] doubleValue];
        
        marker.position = CLLocationCoordinate2DMake(lat, lng);
        marker.title = [aDict objectForKey:@"formatted_address"];
        marker.snippet = [NSString stringWithFormat:@"(%f, %f)", lat, lng];
        marker.map = self.mapView;
        marker.infoWindowAnchor = CGPointMake(0.5, -0.25);
        
        [self.markers addObject:marker];
    }
    
    [self fitMarkersToBounds];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - GMSMapView Delegate

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.title = [NSString stringWithFormat:@"Marker at: %.2f,%.2f",
                    coordinate.latitude, coordinate.longitude];
    marker.position = coordinate;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = _mapView;
    
    // Add the new marker to the list of markers.
    [_markers addObject:marker];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    return false;
}

- (void)fitMarkersToBounds {
    if([[self.data objectForKey:@"displayAll"] boolValue]){
        GMSCoordinateBounds *bounds;
        for (GMSMarker *marker in self.markers) {
            if (bounds == nil) {
                bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:marker.position
                                                              coordinate:marker.position];
            }
            bounds = [bounds includingCoordinate:marker.position];
        }
        GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds
                                                 withPadding:50.0f];
        [self.mapView moveCamera:update];
    }
    else{
        GMSMarker *marker = self.markers[[[self.data objectForKey:@"selectedIndex"] intValue]];
        
        GMSCameraUpdate *move = [GMSCameraUpdate setTarget:marker.position zoom:12];
        [_mapView animateWithCameraUpdate:move];
        
        [self checkDatabase];
    }
}

#pragma mark - Database

- (void)checkDatabase{
    NSArray *marks = [self.data objectForKey:@"marks"];
    
    NSDictionary *aLocation = marks[[[self.data objectForKey:@"selectedIndex"] intValue]];
    
    Boolean isLocationSaved = [[DatabaseController sharedInstance] isLocationSaved:aLocation];
    
    if(isLocationSaved){//Show Delete Button
        UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(deleteSelectedLocation:)];
        
        self.navigationItem.rightBarButtonItem = deleteButton;
    }
    else{//Show Save Button
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveSelectedLocation:)];
        self.navigationItem.rightBarButtonItem = addButton;
    }
}

- (void)saveSelectedLocation:(id)sender {
    NSArray *marks = [self.data objectForKey:@"marks"];
    
    NSDictionary *aLocation = marks[[[self.data objectForKey:@"selectedIndex"] intValue]];
    
    [[DatabaseController sharedInstance] insertLocation:aLocation];
    
    UIBarButtonItem *deleteButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStylePlain target:self action:@selector(deleteSelectedLocation:)];
    
    self.navigationItem.rightBarButtonItem = deleteButton;
}

- (void)deleteSelectedLocation:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kAppName
                                                    message:@"The location will be deleted, are you sure ?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Delete", nil];
    [alert show];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) { //The user confirmed the action
        NSArray *marks = [self.data objectForKey:@"marks"];
        
        NSDictionary *aLocation = marks[[[self.data objectForKey:@"selectedIndex"] intValue]];
        
        [[DatabaseController sharedInstance] deleteLocation:aLocation];
        
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveSelectedLocation:)];
        self.navigationItem.rightBarButtonItem = addButton;
    }
}

@end
