//
//  MapsViewController.m
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import <UIKit/UIKit.h>

@import GoogleMaps;

@interface MapsViewController : UIViewController

@property (strong, nonatomic) NSDictionary *data;

@end

