//
//  Location.m
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import "Location.h"

@implementation Location

@dynamic lastUpdated;
@dynamic formatted_address;
@dynamic lat;
@dynamic lng;

@end
