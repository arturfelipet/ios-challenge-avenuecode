//
//  LocationVO.h
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseVO.h"

@interface LocationVO : BaseVO

@property (nonatomic, retain) NSString *formatted_address;
@property (nonatomic, retain) NSNumber *lat;
@property (nonatomic, retain) NSNumber *lng;

+ (NSDictionary *)initWith:(Location*)model;

- (id)initWith:(Location*)model;

@end
