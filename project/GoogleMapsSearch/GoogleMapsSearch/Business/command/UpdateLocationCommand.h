//
//  UpdateLocationCommand.h
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//


typedef void (^UpdateLocationResponseBlock) (NSDictionary *result, NSError *error);

@protocol UpdateLocationCommandDelegate <NSObject>

@required
- (void)command:(id)command didUpdateLocation:(NSArray *)histories WithError:(NSError*)error;

@end

@interface UpdateLocationCommand : NSObject

@property (nonatomic,weak) id<UpdateLocationCommandDelegate> delegate;

+ (instancetype)sharedInstance;
+ (void)updateSearch:(LocationVO *)aLocationVO WithBlock:(UpdateLocationResponseBlock)responseBlock;

- (id)initWithDelegate:(id<UpdateLocationCommandDelegate>)mDelegate;

@end
