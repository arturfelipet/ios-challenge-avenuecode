//
//  DeleteLocationCommand.h
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^DeleteLocationResponseBlock) (NSDictionary *result, NSError *error);

@protocol DeleteLocationCommandDelegate <NSObject>

@required
- (void)command:(id)command didDeleteLocation:(NSArray *)histories WithError:(NSError*)error;

@end

@interface DeleteLocationCommand : NSObject

@property (nonatomic,weak) id<DeleteLocationCommandDelegate> delegate;

+ (instancetype)sharedInstance;
+ (void)deleteLocation:(LocationVO *)aLocationVO WithBlock:(DeleteLocationResponseBlock)responseBlock;

- (id)initWithDelegate:(id<DeleteLocationCommandDelegate>)mDelegate;

@end
