//
//  ListLocationCommand.m
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import "ListLocationCommand.h"

@implementation ListLocationCommand

+ (instancetype)sharedInstance
{
    static ListLocationCommand *instance = nil;
    static dispatch_once_t onceToken;
    
    if (instance) return instance;
    
    dispatch_once(&onceToken, ^{
        instance = [ListLocationCommand alloc];
        instance = [instance init];
    });
    
    return instance;
}

- (id)initWithDelegate:(id<ListLocationCommandDelegate>)mDelegate
{
    self = [self init];
    if (self)
    {
        self.delegate = mDelegate;
    }
    return self;
}

+ (void)listLocationsWithBlock:(ListLocationResponseBlock)responseBlock{
        
    dispatch_queue_t backgroundQueue = dispatch_queue_create([[NSString stringWithFormat:@"GoogleMapsSearch.%@", NSStringFromClass([self class])] UTF8String], NULL);
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    
    dispatch_async(backgroundQueue, ^
                   {
                       NSError *error = nil;
                       
                       [DAOFactory configDatabaseManager];
                       
                       id result = [[DatabaseManager sharedInstance] fetchData:ENTITY_LOCATION predicate:[NSPredicate predicateWithFormat:@"formatted_address != nil"] offset:0 limit:0 sortBy:@"lastUpdated" ascending:NO error:&error];
                       
                       NSMutableArray* vos = [NSMutableArray array];
                       
                       if ([result isKindOfClass:[NSArray class]])
                           for (Location *aLocation in result){
                               [vos addObject:[[LocationVO alloc] initWith:aLocation]];
                           }
                       
                       dispatch_async(mainQueue, ^
                                      {                                                                              
                                          if (responseBlock) responseBlock(@{@"result": [vos copy]}, error);
                                      });
                   });
    
}

@end
