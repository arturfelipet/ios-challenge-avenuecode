//
//  InsertLocationCommand.h
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

typedef void (^InsertLocationResponseBlock) (NSDictionary *result, NSError *error);

@protocol InsertLocationCommandDelegate <NSObject>

@required
- (void)command:(id)command didInsertSearch:(Location *)aLocation WithError:(NSError*)error;

@end


@interface InsertLocationCommand : NSObject

@property (nonatomic,weak) id<InsertLocationCommandDelegate> delegate;

+ (instancetype)sharedInstance;
+ (void)createLocationWithTitle:(NSString *)formatted_address withLatitude:(NSNumber *)lat withLongitude:(NSNumber *)lng withBlock:(InsertLocationResponseBlock)responseBlock;

- (id)initWithDelegate:(id<InsertLocationCommandDelegate>)mDelegate;

@end
