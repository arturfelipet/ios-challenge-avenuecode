//
//  ListLocationCommand.h
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/7/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

typedef void (^ListLocationResponseBlock) (NSDictionary *result, NSError *error);

@protocol ListLocationCommandDelegate <NSObject>

@required
- (void)command:(id)command didListHistories:(NSArray *) histories WithError:(NSError*)error;

@end


@interface ListLocationCommand : NSObject

@property (nonatomic,weak) id<ListLocationCommandDelegate> delegate;

+ (instancetype)sharedInstance;
+ (void)listLocationsWithBlock:(ListLocationResponseBlock)responseBlock;

- (id)initWithDelegate:(id<ListLocationCommandDelegate>)mDelegate;

@end
