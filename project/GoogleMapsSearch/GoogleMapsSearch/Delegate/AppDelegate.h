//
//  AppDelegate.h
//  GoogleMapsSearch
//
//  Created by Artur Felipe on 12/6/16.
//  Copyright © 2016 Artur Felipe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

