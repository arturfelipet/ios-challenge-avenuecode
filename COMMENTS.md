
1) Main architecture decisions you've made and a quick explanation of why.

Decided to organize the root project by 3 parts: Infrastructure, Business and Interface.

- Infrastructure: Contains much of the low level base classes for management of Database, VO, DAO. Normally external frameworks are also here if they are low level like: Reachability and Unirest.
- Business: Contains a mid level architecture stuff like database models and commands.
- Interface: Contains most of the Views and ViewControllers.


2) List of third party libraries and why you chose each one

- Google Maps Framework as was asked.
- Reachability to check internet conditions.


3) What could be improved if you had more time.

- Transform Infrastructure in an umbrella frameworks that can be imported easily in most projects.
- Transform Business in a framework also with Business related matters.
- Use location service to add as an option for search places if looking for nearby.
- Add a View with search history so the user can redo searches easily.
- Move webserver search and parse to Business.
- Include street view support.
- Include a mediator to aid on the low coupling between views so they can be changed with much less recoding.

4) Mention anything that was asked but not delivered and why, and any additional comments.
